package com.yoavst.halufon

import com.github.kittinunf.fuel.Fuel
import com.yoavst.halufon.controller.Storage
import com.yoavst.halufon.controller.Updater
import com.yoavst.halufon.controller.UpdaterImpl
import org.junit.Before
import org.junit.Test
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.addSingleton
import kotlin.test.assertNotEquals
import kotlin.test.fail

class UpdaterTest {
    val updater: Updater = UpdaterImpl

    @Before
    fun setUp() {
        Injekt.addSingleton<Storage>(FakeStorage)
        Fuel.testMode {
            timeout = 3000
            blocking = true
        }
    }

    @Test
    fun update() {
        updater[0] = object : Updater.Callback {
            override fun onSuccess() {
                val words = FakeStorage.words
                assertNotEquals(0, words.size)
            }

            override fun onFail() {
                fail("Update should not fail")
            }
        }
        updater.update()
    }
}