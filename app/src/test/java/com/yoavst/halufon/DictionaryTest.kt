package com.yoavst.halufon

import com.yoavst.halufon.controller.DictionaryImpl
import com.yoavst.halufon.model.Word
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals
import kotlin.test.assertTrue

class DictionaryTest {
    val words = listOf(
            Word(1, "אובייקט", "מֻשָּׂא; עֶצֶם"),
            Word(2, "אַייקון", "אִיקוֹן"),
            Word(3, "בּוֹיְדֶם, בוידעם", "עֲלִיַּת תִּקְרָה"),
            Word(4, "ביי", "להתראות")

    )
    val dictionary = DictionaryImpl(words)

    @Test
    fun getId() {
        assertEquals(words[0], dictionary[1])
        assertEquals(words[1], dictionary[2])
        assertEquals(words[2], dictionary[3])
    }

    @Test
    fun getByLetter1() {
        val data = dictionary['א']
        assertEquals(2, data.size)
        assertTrue(words[0] in data)
        assertTrue(words[1] in data)
    }

    @Test
    fun getByLetter2() {
        val data = dictionary['ב']
        assertEquals(2, data.size)
        assertTrue(words[2] in data)
        assertTrue(words[3] in data)
    }

    @Test
    fun getByLetter3() {
        val data = dictionary['ע', false]
        assertEquals(1, data.size)
        assertTrue(words[2] in data)
    }

    @Test
    fun search() {
        val data = dictionary.search("ביי")
        assertEquals(2, data.size)
        assertTrue(words[0] in data)
        assertTrue(words[3] in data)
    }

    @Test
    fun random() {
        assertNotEquals(dictionary.random(), dictionary.random())
    }
}