package com.yoavst.halufon

import com.github.salomonbrys.kotson.fromJson
import com.google.gson.Gson
import com.yoavst.halufon.controller.Storage
import com.yoavst.halufon.model.Word

object FakeStorage : Storage {
    override fun set(id: Int, callback: () -> Unit) = Unit
    override fun minusAssign(id: Int) = Unit

    val gson = Gson()
    override var isForeign: Boolean = false
    override var words: List<Word> = emptyList()

    override fun saveWords(data: ByteArray) {
        words = gson.fromJson<List<Word>>(data.inputStream().reader())
    }
}