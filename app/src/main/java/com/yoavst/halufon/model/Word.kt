package com.yoavst.halufon.model

import com.google.gson.annotations.SerializedName

data class Word(val id: Int, val word: String, @SerializedName("word_hebrew") val hebrew: String, val explanation: String? = null)