package com.yoavst.halufon.injection

import com.yoavst.halufon.controller.*
import uy.kohesive.injekt.api.InjektModule
import uy.kohesive.injekt.api.InjektRegistrar
import uy.kohesive.injekt.api.addSingleton

object InjectionModule : InjektModule {
    override fun InjektRegistrar.registerInjectables() {
        addSingleton<Storage>(StorageWrapper(StorageImpl))
        addSingleton<Updater>(UpdaterImpl)
        addSingleton<Dictionary>(DictionaryStorageImpl())
    }
}