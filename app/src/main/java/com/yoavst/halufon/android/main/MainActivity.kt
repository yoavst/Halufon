package com.yoavst.halufon.android.main

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Gravity
import android.view.View
import android.widget.EditText
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog
import com.yoavst.halufon.R
import com.yoavst.halufon.android.hide
import com.yoavst.halufon.android.isNetworkAvailable
import com.yoavst.halufon.android.loading.LoadingActivity
import com.yoavst.halufon.android.show
import com.yoavst.halufon.android.util.ModifiedFloatingSearchView
import com.yoavst.halufon.controller.Dictionary
import com.yoavst.halufon.controller.Storage
import com.yoavst.halufon.controller.Updater
import com.yoavst.halufon.model.Word
import kotlinx.android.synthetic.main.main_layout.*
import org.jetbrains.anko.*
import uy.kohesive.injekt.injectLazy

class MainActivity : AppCompatActivity(), Updater.Callback {
    private val storage: Storage by injectLazy()
    private val dictionary: Dictionary by injectLazy()
    private val updater: Updater by injectLazy()

    private var currentLetter: Char? = null
    private var currentSearchTerm: String? = null

    private var adapter: WordsAdapter? = null
    private var currentDialog: MaterialStyledDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (storage.words.isEmpty()) {
            overridePendingTransition(0, 0)
            startActivity<LoadingActivity>()
            finish()
        } else {
            setContentView(R.layout.main_layout)
            initViews()
            setSelectedView('א')
            onItemSelected(dictionary.random())
            updater[Id] = this
        }
    }

    override fun onPause() {
        super.onPause()
        currentDialog?.dismiss()
        currentDialog = null
    }


    override fun onDestroy() {
        super.onDestroy()
        updater -= Id
    }

    private fun initViews() {
        navigationView.layoutManager = LinearLayoutManager(this)
        navigationView.setHasFixedSize(true)
        navigationView.adapter = AlphabeticalAdapter {
            setSelectedView(it)
        }

        itemsView.layoutManager = LinearLayoutManager(this)
        itemsView.setHasFixedSize(true)

        expandButton.onClick {
            toggleDataLayout()
        }
        wordCollapsed.onClick {
            expandDataLayout()
        }

        wordView.onClick {
            //FIXME
        }

        randomButton.onClick {
            onItemSelected(dictionary.random(), openView = false)
        }

        find<EditText>(R.id.search_bar_text).gravity = Gravity.RIGHT or Gravity.CENTER_VERTICAL
        updateMenu()
        searchView.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.refresh -> {
                    if (isNetworkAvailable())
                        updater.update()
                    else
                        toast(R.string.no_internet)
                }
                R.id.modeToggle -> {
                    storage.isForeign = !storage.isForeign
                    adapter?.isForeign = storage.isForeign
                    updateMenu()
                    updateView()

                }
                R.id.thanks -> {
                    currentDialog = MaterialStyledDialog(this)
                            .setTitle(getString(R.string.thanks))
                            .setDescription(getString(R.string.our_thanks))
                            .setCancelable(true)
                            .setIcon(R.drawable.ic_love)
                            .setPositive(getString(R.string.ok)) { dialog, _ ->
                                dialog.dismiss()
                                currentDialog = null
                            }.show()
                }
                R.id.about -> {
                    currentDialog = MaterialStyledDialog(this)
                            .setTitle(getString(R.string.about))
                            .setDescription(getString(R.string.about_info))
                            .setCancelable(true)
                            .setIcon(R.drawable.ic_info)
                            .setPositive(getString(R.string.ok)) { dialog, _ ->
                                dialog.dismiss()
                                currentDialog = null
                            }.setNeutral(getString(R.string.my_website)) { dialog, _ ->
                                dialog.dismiss()
                                currentDialog = null
                                browse("http://yoavst.com")
                    }
                            .show()
                }
            }
        }

        searchView.setOnQueryChangeListener { _, newQuery ->
            setSelectedView(newQuery)
        }

        searchView.setOnSearchListener(object : ModifiedFloatingSearchView.OnSearchListener {
            override fun onSearchAction() {
                setSelectedView(searchView.query)
            }

            override fun onSuggestionClicked(searchSuggestion: SearchSuggestion) = Unit
        })

    }

    private fun updateMenu() {
        if (storage.isForeign) {
            searchView.inflateOverflowMenu(R.menu.menu_main)
        } else {
            searchView.inflateOverflowMenu(R.menu.menu_main_hebrew)
        }
    }

    private fun setSelectedView(letter: Char) {
        currentLetter = letter
        currentSearchTerm = null
        searchView.query = ""
        val data = dictionary[letter, storage.isForeign]
        if (adapter == null) {
            adapter = WordsAdapter(data, storage.isForeign) { onItemSelected(it) }
            itemsView.adapter = adapter
        } else {
            adapter?.apply {
                words = data
                notifyDataSetChanged()
            }
        }
    }

    private fun setSelectedView(query: String) {
        currentLetter = null
        currentSearchTerm = query
        val data = dictionary.search(query, storage.isForeign)
        if (adapter == null) {
            adapter = WordsAdapter(data, storage.isForeign) { onItemSelected(it) }
            itemsView.adapter = adapter
        } else {
            adapter?.apply {
                words = data
                notifyDataSetChanged()
            }
        }
    }


    private fun onItemSelected(word: Word, openView: Boolean = true) {
        if (openView)
            expandDataLayout()
        wordNotInHebrew.text = word.word
        wordCollapsed.text = word.word

        wordInHebrew.text = word.hebrew
        if (word.explanation.isNullOrEmpty())
            explainLayout.hide()
        else {
            explainLayout.show()
            explanation.text = word.explanation?.trim()
        }
    }

    private fun expandDataLayout() {
        dataLayout.show()
        wordCollapsed.hide()
        expandButton.imageResource = R.drawable.ic_expand_less
    }

    private fun collapseDataLayout() {
        dataLayout.hide()
        wordCollapsed.show()
        expandButton.imageResource = R.drawable.ic_expand_more
    }

    private fun toggleDataLayout() {
        if (dataLayout.visibility == View.VISIBLE)
            collapseDataLayout()
        else expandDataLayout()
    }

    override fun onBackPressed() {
        if (searchView.isSearchBarFocused)
            searchView.clearSearchFocus()
        else
            super.onBackPressed()
    }

    override fun onSuccess() {
        toast(R.string.updated)
        updateView()
    }

    override fun onFail() {
        toast(R.string.failed_to_update)
    }

    private fun updateView() {
        if (currentLetter != null)
            setSelectedView(currentLetter!!)
        else if (currentSearchTerm != null)
            setSelectedView(currentSearchTerm!!)

    }

    companion object {
        private val Id = 8347
    }
}