package com.yoavst.halufon.android.loading

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import com.yoavst.halufon.R
import com.yoavst.halufon.android.hide
import com.yoavst.halufon.android.isNetworkAvailable
import com.yoavst.halufon.android.lollipop
import com.yoavst.halufon.android.main.MainActivity
import com.yoavst.halufon.android.show
import com.yoavst.halufon.controller.Storage
import com.yoavst.halufon.controller.Updater
import kotlinx.android.synthetic.main.loading_layout.*
import org.jetbrains.anko.onClick
import org.jetbrains.anko.startActivity
import uy.kohesive.injekt.injectLazy

class LoadingActivity : AppCompatActivity(), Updater.Callback {
    private val storage: Storage by injectLazy()
    private val updater: Updater by injectLazy()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.loading_layout)
        initViews()
        loadData()
    }

    override fun onDestroy() {
        super.onDestroy()
        updater -= Id
    }

    private fun initViews() {
        lollipop {
            window.navigationBarColor = ContextCompat.getColor(this, R.color.colorPrimary)
        }
        tryAgain.onClick {
            loadData()
        }
    }

    private fun showLoading() {
        tryAgain.hide()
    }

    private fun showError(isConnectionError: Boolean) {
        tryAgain.show()
    }

    private fun showDone() {
        skip()
    }

    private fun skip() {
        overridePendingTransition(0, 0)
        startActivity<MainActivity>()
        finish()
    }


    fun loadData() {
        if (storage.words.isNotEmpty())
            skip()
        else if (isNetworkAvailable()) {
            showLoading()
            updater[Id] = this
            updater.update()
        } else {
            showError(true)
        }
    }


    override fun onSuccess() {
        showDone()
    }

    override fun onFail() {
        showError(false)
    }

    companion object {
        private val Id = 1253
    }

}