package com.yoavst.halufon.android

import android.app.Application
import com.chibatching.kotpref.Kotpref
import com.yoavst.halufon.injection.InjectionModule
import uy.kohesive.injekt.Injekt

class App: Application() {
    override fun onCreate() {
        super.onCreate()
        Instance = this
        initComponents()
    }

    private fun initComponents() {
        try {
            Kotpref.init(this)
            Injekt.importModule(InjectionModule)
        } catch (e: Exception) {
            e.printStackTrace()
            throw IllegalAccessError()
        }
    }


    companion object {
        lateinit var Instance: App
    }
}