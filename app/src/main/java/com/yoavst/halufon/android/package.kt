package com.yoavst.halufon.android

import android.os.Build
import android.view.View
import org.jetbrains.anko.connectivityManager

fun isNetworkAvailable() = App.Instance.connectivityManager.activeNetworkInfo?.isConnected ?: false

fun View.show() {
    visibility = View.VISIBLE
}

fun View.hide() {
    visibility = View.GONE
}

inline fun lollipop(func: () -> Unit) {
    if (Build.VERSION.SDK_INT >= 21)
        func()
}

