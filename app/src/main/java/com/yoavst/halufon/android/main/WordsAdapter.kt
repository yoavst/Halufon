package com.yoavst.halufon.android.main

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import android.widget.TextView
import com.yoavst.halufon.R
import com.yoavst.halufon.model.Word
import org.jetbrains.anko.layoutInflater
import org.jetbrains.anko.onClick

class WordsAdapter(var words: List<Word>, var isForeign: Boolean, val callback: (Word) -> Unit) : RecyclerView.Adapter<WordsAdapter.WordViewHolder>() {

    override fun onBindViewHolder(holder: WordViewHolder, position: Int) {
        holder.textView.text = words[position].let { if (isForeign) it.word else it.hebrew}
        holder.textView.onClick {
            callback(words[position])
        }
    }

    override fun getItemCount(): Int = words.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WordViewHolder {
        return WordViewHolder(parent.context.layoutInflater.inflate(R.layout.word_item, parent, false) as TextView)
    }

    class WordViewHolder(val textView: TextView) : RecyclerView.ViewHolder(textView)
}