package com.yoavst.halufon.android.main

import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import android.widget.TextView
import com.yoavst.halufon.R
import org.jetbrains.anko.layoutInflater
import org.jetbrains.anko.onClick
import org.jetbrains.anko.textColor

class AlphabeticalAdapter(val callback: (Char) -> Unit) : RecyclerView.Adapter<AlphabeticalAdapter.AlphabeticalViewHolder>() {
    val items = "אבגדהוזחטיכלמנסעפצקרשת"
    var selectedPos = 0
    val selectedColor = Color.parseColor("#18FFFF")


    override fun onBindViewHolder(holder: AlphabeticalViewHolder, position: Int) {
        holder.textView.text = items[position].toString()
        holder.textView.textColor = if (selectedPos != position) Color.WHITE else selectedColor

        holder.textView.onClick {
            notifyItemChanged(selectedPos)
            selectedPos = position
            notifyItemChanged(selectedPos)

            callback(holder.textView.text[0])
        }

    }

    override fun getItemCount(): Int = 22

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlphabeticalViewHolder {
        return AlphabeticalViewHolder(parent.context.layoutInflater.inflate(R.layout.alphabetical_item, parent, false) as TextView)
    }

    class AlphabeticalViewHolder(val textView: TextView) : RecyclerView.ViewHolder(textView)
}