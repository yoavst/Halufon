package com.yoavst.halufon.controller

import com.yoavst.halufon.model.Word

class StorageWrapper(private val storage: Storage): Storage by storage{
    override var isForeign: Boolean
        get() {
            if (_isForeign == null) {
                _isForeign = storage.isForeign
            }
            return _isForeign!!
        }
        set(value) {
            _isForeign = value
            storage.isForeign = value
        }

    var _isForeign: Boolean? = null

    override val words: List<Word>
        get() {
            if (_words == null)
                _words = storage.words
            return _words!!
        }

    override fun saveWords(data: ByteArray) {
        _words = null
        storage.saveWords(data)
    }

    var _words: List<Word>? = null
}
