package com.yoavst.halufon.controller

import com.yoavst.halufon.model.Word

interface Dictionary {
    operator fun get(id: Int): Word
    operator fun get(letter: Char, isForeign: Boolean = true): List<Word>

    fun random(): Word

    fun search(text: String, isForeign: Boolean = true): List<Word>
}
