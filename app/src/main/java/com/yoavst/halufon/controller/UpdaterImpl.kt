package com.yoavst.halufon.controller

import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.github.kittinunf.result.flatMap
import com.github.salomonbrys.kotson.fromJson
import com.github.salomonbrys.kotson.get
import com.google.gson.Gson
import com.google.gson.JsonParser
import com.yoavst.halufon.model.Word
import uy.kohesive.injekt.injectLazy
import java.util.*

object UpdaterImpl : Updater {
    private val storage: Storage by injectLazy()
    private val callbacks = HashMap<Int, Updater.Callback>(2)

    override var isBusy: Boolean = false


    override fun set(id: Int, callback: Updater.Callback) {
        callbacks[id] = callback
    }

    override fun minusAssign(id: Int) {
        callbacks.remove(id)
    }


    override fun update(): Boolean {
        if (isBusy) return false
        isBusy = true
        ApiAddress.httpGet().response { request, response, result ->
            result.flatMap {
                processData(it)
            }.fold(success = {
                callbacks.values.forEach {
                    it.onSuccess()
                }
            }, failure = {
                it.printStackTrace()
                callbacks.values.forEach {
                    it.onFail()
                }
            })
            isBusy = false
        }
        return true
    }

    private fun processData(byteArray: ByteArray): Result<Unit, Exception> = Result.of {
        val words = parser.parse(byteArray.inputStream().reader())["words"]
        // Try parse
        val word = gson.fromJson<Word>(words[0])
        // Parse was successful
        storage.saveWords(gson.toJson(words).toByteArray())
    }

    val gson = Gson()
    val parser = JsonParser()

    const val ApiAddress = "http://halufon.hebrew-academy.org.il/words.php"
}