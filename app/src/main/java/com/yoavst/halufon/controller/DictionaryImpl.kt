package com.yoavst.halufon.controller

import com.yoavst.halufon.model.Word
import java.util.*

class DictionaryImpl(val words: List<Word>) : Dictionary {
    val searchIndexForeign = words.map { it.word.strip() }
    val searchIndexHebrew = words.map { it.hebrew.strip() }
    var lastRandomWord: Word? = null

    override fun get(id: Int): Word = words.first { it.id == id }

    override fun get(letter: Char, isForeign: Boolean): List<Word> {
        val results = LinkedList<Word>()

        (if (isForeign) searchIndexForeign else searchIndexHebrew).forEachIndexed { i, s ->
            if (s.startsWith(letter))
                results += words[i]
        }
        return results
    }

    override fun random(): Word {
        var word: Word
        do {
            word = words[random.nextInt(words.size)]
        } while (word == lastRandomWord)
        lastRandomWord = word
        return word
    }

    override fun search(text: String, isForeign: Boolean): List<Word> {
        val keyword = text.strip()
        val results = LinkedList<Word>()

        (if (isForeign) searchIndexForeign else searchIndexHebrew).forEachIndexed { i, s ->
            if (keyword in s)
                results += words[i]
        }
        return results
    }

    companion object {
        private val random = Random()
        private val ValidLetters = 'א'..'ת'
        private val Space = ' '

        private fun String.strip(): String = filter { it == Space || it in ValidLetters }
    }
}