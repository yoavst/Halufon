package com.yoavst.halufon.controller

import com.yoavst.halufon.model.Word

interface Storage {
    var isForeign: Boolean

    val words: List<Word>
    fun saveWords(data: ByteArray)

    operator fun set(id: Int, callback: () -> Unit)
    operator fun minusAssign(id: Int)
}