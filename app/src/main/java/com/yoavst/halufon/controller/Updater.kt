package com.yoavst.halufon.controller

interface Updater {
    operator fun set(id: Int, callback: Callback)
    operator fun minusAssign(id: Int)

    val isBusy: Boolean
    fun update(): Boolean

    interface Callback {
        fun onSuccess()
        fun onFail()
    }
}