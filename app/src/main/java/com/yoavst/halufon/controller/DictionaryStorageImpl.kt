package com.yoavst.halufon.controller

import com.yoavst.halufon.model.Word
import uy.kohesive.injekt.injectLazy

class DictionaryStorageImpl() : Dictionary {
    val storage: Storage by injectLazy()
    private lateinit var dictionary: DictionaryImpl

    init {
        storage[Id] = { initData() }
        initData()
    }


    private fun initData() {
        dictionary = DictionaryImpl(storage.words)
    }

    override fun get(id: Int): Word = dictionary[id]

    override fun get(letter: Char, isForeign: Boolean): List<Word> = dictionary[letter, isForeign]

    override fun random(): Word = dictionary.random()

    override fun search(text: String, isForeign: Boolean): List<Word> = dictionary.search(text, isForeign)

    companion object {
        val Id = 842
    }
}