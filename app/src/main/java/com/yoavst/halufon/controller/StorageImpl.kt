package com.yoavst.halufon.controller

import com.chibatching.kotpref.KotprefModel
import com.github.salomonbrys.kotson.fromJson
import com.google.gson.Gson
import com.yoavst.halufon.model.Word
import java.io.File
import java.util.*

object StorageImpl : KotprefModel(), Storage {
    override var isForeign by booleanPrefVar(default = true)
    private val callbacks = HashMap<Int, () -> Unit>(2)

    override fun set(id: Int, callback: () -> Unit) {
        callbacks[id] = callback
    }

    override fun minusAssign(id: Int) {
        callbacks.remove(id)
    }

    override val words: List<Word>
        get() {
            if (!WordsFile.exists()) return emptyList()
            else return gson.fromJson(WordsFile.reader())
        }

    override fun saveWords(data: ByteArray) {
        if (data.isEmpty()) WordsFile.delete()
        else {
            WordsFile.parentFile.mkdir()
            WordsFile.writeBytes(data)
        }

        callbacks.values.forEach { it() }
    }

    val WordsFile = File("/data/data/com.yoavst.halufon/files/words.json")
    val gson = Gson()
}